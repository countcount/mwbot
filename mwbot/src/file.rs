/*
Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::upload::UploadRequest;
use crate::{Page, Result};

/// Represents a on-wiki file page.
#[derive(Debug)]
pub struct File {
    page: Page,
}

impl File {
    pub(crate) fn new(page: &Page) -> Self {
        assert!(page.is_file());
        Self { page: page.clone() }
    }

    /// Upload a new file
    pub async fn upload(self, req: UploadRequest) -> Result<Self> {
        let params = req.params();
        let filename = self
            .page
            .bot
            .api()
            .upload(
                self.page.title(),
                req.file,
                req.chunk_size,
                req.ignore_warnings,
                params,
            )
            .await?;
        Ok(self
            .page
            .bot
            .page(&filename)?
            .as_file()
            .expect("MediaWiki API returned non-file title"))
    }
}
