/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::parsoid::{ImmutableWikicode, Wikicode};
use mwapi_responses::timestamp::Timestamp;
use serde::Deserialize;

/// Extra options for saving pages
pub struct SaveOptions {
    pub(crate) summary: String,
    pub(crate) mark_as_bot: Option<bool>,
    pub(crate) minor: Option<bool>,
    pub(crate) tags: Vec<String>,
}

impl SaveOptions {
    /// Create options with the given summary
    pub fn summary(summary: &str) -> Self {
        Self {
            summary: summary.to_string(),
            mark_as_bot: None,
            minor: None,
            tags: vec![],
        }
    }

    /// Change the edit summary
    pub fn set_summary(mut self, summary: &str) -> Self {
        self.summary = summary.to_string();
        self
    }

    /// Overide the default mark_as_bot option with the given value
    pub fn mark_as_bot(mut self, mark: bool) -> Self {
        self.mark_as_bot = Some(mark);
        self
    }

    /// Mark the edit as minor, or override the "Mark all
    /// edits as minor by default" preference to not mark
    /// it as minor.
    pub fn mark_as_minor(mut self, mark: bool) -> Self {
        self.minor = Some(mark);
        self
    }

    /// Add the given change tag to the edit
    pub fn add_tag(mut self, tag: &str) -> Self {
        self.tags.push(tag.to_string());
        self
    }
}

pub enum Saveable {
    Wikitext(String),
    Html(ImmutableWikicode),
}

impl From<String> for Saveable {
    fn from(wikitext: String) -> Self {
        Self::Wikitext(wikitext)
    }
}

impl From<&str> for Saveable {
    fn from(wikitext: &str) -> Self {
        Self::Wikitext(wikitext.to_string())
    }
}

impl From<&String> for Saveable {
    fn from(wikitext: &String) -> Self {
        Self::Wikitext(wikitext.to_string())
    }
}

impl From<Wikicode> for Saveable {
    fn from(html: Wikicode) -> Self {
        html.into_immutable().into()
    }
}

impl From<ImmutableWikicode> for Saveable {
    fn from(html: ImmutableWikicode) -> Self {
        Self::Html(html)
    }
}

/// Response from action=edit
///
/// TODO: Move to mwapi_responses
#[derive(Deserialize, Clone, Debug)]
pub struct EditResponse {
    pub contentmodel: String,
    pub newrevid: Option<u64>,
    pub newtimestamp: Option<Timestamp>,
    pub oldrevid: Option<u64>,
    #[serde(default)]
    pub nochange: bool,
    #[serde(default)]
    pub new: bool,
    pub pageid: u32,
    pub result: String,
    pub title: String,
}
