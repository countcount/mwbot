/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::WikinodeIterator;
use kuchiki::NodeRef;

/// Trait for Wikinodes that actually span multiple elements
pub trait WikiMultinode {
    fn as_nodes(&self) -> Vec<NodeRef>;

    /// Remove this from the document
    fn detach(&self) {
        for node in self.as_nodes() {
            node.detach();
        }
    }
}

impl<T: WikinodeIterator> WikiMultinode for T {
    fn as_nodes(&self) -> Vec<NodeRef> {
        vec![self.as_node().clone()]
    }
}
