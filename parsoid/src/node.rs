/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//! Wikinodes represent various MediaWiki syntax constructs

use crate::gallery::Gallery;
use crate::image::Image;
use crate::inclusion::IncludeOnlyDataMw;
use crate::indicator::{IndicatorAttrs, IndicatorBody, IndicatorDataMw};
use crate::private::Sealed;
use crate::{
    assert_element, clean_link, full_link, inner_data, parse_target,
    set_inner_data, Error, Result, Wikicode, WikinodeIterator,
};
use kuchiki::{Attribute, ExpandedName, NodeRef};
use std::ops::Deref;
use urlencoding::{decode, encode};

/// Enum that represents all the different types of nodes
#[derive(Debug, Clone)]
pub enum Wikinode {
    BehaviorSwitch(BehaviorSwitch),
    Category(Category),
    /// HTML comment
    Comment(Comment),
    DisplaySpace(DisplaySpace),
    /// External link
    ExtLink(ExtLink),
    Gallery(Gallery),
    Heading(Heading),
    HtmlEntity(HtmlEntity),
    Image(Image),
    IncludeOnly(IncludeOnly),
    Indicator(Indicator),
    InterwikiLink(InterwikiLink),
    LanguageLink(LanguageLink),
    Nowiki(Nowiki),
    Placeholder(Placeholder),
    Redirect(Redirect),
    Section(Section),
    /// Wiki (internal) link
    WikiLink(WikiLink),
    /// A generic HTML node that we haven't implemented a specific type for yet
    /// or doesn't need one.
    Generic(Wikicode),
}

impl Deref for Wikinode {
    type Target = NodeRef;

    fn deref(&self) -> &Self::Target {
        self.as_node()
    }
}

impl Wikinode {
    pub(crate) fn new_from_node(node: &NodeRef) -> Self {
        if node.as_comment().is_some() {
            return Comment::new_from_node(node).into();
        }
        if let Some(element) = node.as_element() {
            let tag_name = element.name.local.clone();
            let attributes = element.attributes.borrow();
            if let Some(typeof_) = attributes.get("typeof") {
                if typeof_ == "mw:Placeholder"
                    || typeof_.starts_with("mw:Placeholder/")
                {
                    return Placeholder::new_from_node(node).into();
                }
            }
            match tag_name {
                local_name!("a") => {
                    if let Some(rel) = attributes.get("rel") {
                        // need to check interwiki first since the rel is a substring
                        // of the wikilink one
                        if rel.contains(InterwikiLink::REL) {
                            return InterwikiLink::new_from_node(node).into();
                        } else if rel.contains(WikiLink::REL) {
                            return WikiLink::new_from_node(node).into();
                        } else if rel.contains(ExtLink::REL) {
                            return ExtLink::new_from_node(node).into();
                        }
                        // fall through to generic
                    }
                }
                local_name!("h1")
                | local_name!("h2")
                | local_name!("h3")
                | local_name!("h4")
                | local_name!("h5")
                | local_name!("h6") => {
                    return Heading::new_from_node(node).into();
                }
                local_name!("link") => {
                    if let Some(rel) = attributes.get("rel") {
                        match rel {
                            Category::REL => {
                                return Category::new_from_node(node).into();
                            }
                            LanguageLink::REL => {
                                return LanguageLink::new_from_node(node)
                                    .into();
                            }
                            Redirect::REL => {
                                return Redirect::new_from_node(node).into();
                            }
                            // fall through to generic
                            _ => {}
                        }
                    }
                    if let Some(typeof_) = attributes.get("typeof") {
                        if typeof_ == Indicator::TYPEOF {
                            return Indicator::new_from_node(node).into();
                        }
                    }
                }
                local_name!("meta") => {
                    if let Some(property) = attributes.get("property") {
                        if property.starts_with("mw:PageProp/") {
                            return BehaviorSwitch::new_from_node(node).into();
                        }
                    } else if let Some(typeof_) = attributes.get("typeof") {
                        if typeof_ == IncludeOnly::TYPEOF {
                            return IncludeOnly::new_from_node(node).into();
                        }
                    }
                }
                local_name!("section") => {
                    if attributes.contains("data-mw-section-id") {
                        return Section::new_from_node(node).into();
                    }
                }
                local_name!("span") => {
                    if let Some(type_of) = attributes.get("typeof") {
                        if type_of == Nowiki::TYPEOF {
                            return Nowiki::new_from_node(node).into();
                        } else if type_of == HtmlEntity::TYPEOF {
                            return HtmlEntity::new_from_node(node).into();
                        } else if type_of == DisplaySpace::TYPEOF {
                            return DisplaySpace::new_from_node(node).into();
                        } else if type_of.starts_with(Image::TYPEOF_PREFIX) {
                            return Image::new_from_node(node).into();
                        }
                    }
                }
                local_name!("ul") => {
                    if let Some(type_of) = attributes.get("typeof") {
                        if type_of == Gallery::TYPEOF {
                            return Gallery::new_from_node(node).into();
                        }
                    }
                }
                // fall through to generic
                _ => {}
            }
        }

        Self::Generic(Wikicode::new_from_node(node))
    }

    pub fn as_behavior_switch(&self) -> Option<BehaviorSwitch> {
        match self {
            Self::BehaviorSwitch(switch) => Some(switch.clone()),
            _ => None,
        }
    }

    pub fn as_category(&self) -> Option<Category> {
        match self {
            Self::Category(category) => Some(category.clone()),
            _ => None,
        }
    }

    /// If this node is a comment, get a clone of it
    pub fn as_comment(&self) -> Option<Comment> {
        match self {
            Self::Comment(comment) => Some(comment.clone()),
            _ => None,
        }
    }

    pub fn as_displayspace(&self) -> Option<DisplaySpace> {
        match self {
            Self::DisplaySpace(displayspace) => Some(displayspace.clone()),
            _ => None,
        }
    }

    /// If this node is an external link, get a clone of it
    pub fn as_extlink(&self) -> Option<ExtLink> {
        match self {
            Self::ExtLink(extlink) => Some(extlink.clone()),
            _ => None,
        }
    }

    /// If this node is a gallery, get a clone of it
    pub fn as_gallery(&self) -> Option<Gallery> {
        match self {
            Self::Gallery(gallery) => Some(gallery.clone()),
            _ => None,
        }
    }

    /// If this node is generic, get a clone of it
    pub fn as_generic(&self) -> Option<Wikicode> {
        match self {
            Self::Generic(node) => Some(node.clone()),
            _ => None,
        }
    }

    pub fn as_heading(&self) -> Option<Heading> {
        match self {
            Self::Heading(heading) => Some(heading.clone()),
            _ => None,
        }
    }

    pub fn as_html_entity(&self) -> Option<HtmlEntity> {
        match self {
            Self::HtmlEntity(entity) => Some(entity.clone()),
            _ => None,
        }
    }

    pub fn as_image(&self) -> Option<Image> {
        match self {
            Self::Image(image) => Some(image.clone()),
            _ => None,
        }
    }

    pub fn as_includeonly(&self) -> Option<IncludeOnly> {
        match self {
            Self::IncludeOnly(includeonly) => Some(includeonly.clone()),
            _ => None,
        }
    }

    pub fn as_indicator(&self) -> Option<Indicator> {
        match self {
            Self::Indicator(indicator) => Some(indicator.clone()),
            _ => None,
        }
    }

    pub fn as_interwiki_link(&self) -> Option<InterwikiLink> {
        match self {
            Self::InterwikiLink(link) => Some(link.clone()),
            _ => None,
        }
    }

    pub fn as_language_link(&self) -> Option<LanguageLink> {
        match self {
            Self::LanguageLink(link) => Some(link.clone()),
            _ => None,
        }
    }

    pub fn as_nowiki(&self) -> Option<Nowiki> {
        match self {
            Self::Nowiki(nowiki) => Some(nowiki.clone()),
            _ => None,
        }
    }

    pub fn as_placeholder(&self) -> Option<Placeholder> {
        match self {
            Self::Placeholder(placeholder) => Some(placeholder.clone()),
            _ => None,
        }
    }

    pub fn as_redirect(&self) -> Option<Redirect> {
        match self {
            Self::Redirect(redirect) => Some(redirect.clone()),
            _ => None,
        }
    }

    pub fn as_section(&self) -> Option<Section> {
        match self {
            Self::Section(section) => Some(section.clone()),
            _ => None,
        }
    }

    /// If this node is a wiki link, get a clone of it
    pub fn as_wikilink(&self) -> Option<WikiLink> {
        match self {
            Self::WikiLink(wikilink) => Some(wikilink.clone()),
            _ => None,
        }
    }
}

impl Sealed for Wikinode {}

impl WikinodeIterator for Wikinode {
    fn as_node(&self) -> &NodeRef {
        // This relies on all the types deref to NodeRef
        match self {
            Self::BehaviorSwitch(switch) => switch,
            Self::Category(category) => category,
            Self::Comment(comment) => comment,
            Self::DisplaySpace(displayspace) => displayspace,
            Self::ExtLink(extlink) => extlink,
            Self::Gallery(gallery) => gallery,
            Self::Heading(heading) => heading,
            Self::HtmlEntity(entity) => entity,
            Self::Image(image) => image,
            Self::IncludeOnly(includeonly) => includeonly,
            Self::Indicator(indicator) => indicator,
            Self::InterwikiLink(link) => link,
            Self::LanguageLink(link) => link,
            Self::Nowiki(nowiki) => nowiki,
            Self::Placeholder(placeholder) => placeholder,
            Self::Redirect(redirect) => redirect,
            Self::Section(section) => section,
            Self::WikiLink(wikilink) => wikilink,
            Self::Generic(code) => code,
        }
    }
}

macro_rules! impl_traits {
    ( $name:ident ) => {
        impl From<$name> for Wikinode {
            fn from(node: $name) -> Self {
                Self::$name(node)
            }
        }

        impl Deref for $name {
            type Target = NodeRef;

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl WikinodeIterator for $name {
            fn as_node(&self) -> &NodeRef {
                &self.0
            }
        }

        impl Sealed for $name {}
    };
}

impl_traits!(BehaviorSwitch);
impl_traits!(Category);
impl_traits!(Comment);
impl_traits!(DisplaySpace);
impl_traits!(ExtLink);
impl_traits!(Gallery);
impl_traits!(Heading);
impl_traits!(HtmlEntity);
impl_traits!(Image);
impl_traits!(IncludeOnly);
impl_traits!(Indicator);
impl_traits!(InterwikiLink);
impl_traits!(LanguageLink);
impl_traits!(Nowiki);
impl_traits!(Placeholder);
impl_traits!(Redirect);
impl_traits!(Section);
impl_traits!(WikiLink);

/// Represents a wikitext/HTML comment.
/// ```
/// # use parsoid::prelude::*;
/// let comment = Comment::new("foo");
/// assert_eq!(comment.text(), "foo".to_string());
/// assert_eq!(comment.to_string(), "<!--foo-->".to_string());
/// comment.set_text("bar");
/// assert_eq!(comment.to_string(), "<!--bar-->".to_string());
/// ```
#[derive(Debug, Clone)]
pub struct Comment(NodeRef);

impl Comment {
    /// Create a new `Comment`, with the given text
    pub fn new(text: &str) -> Self {
        Self(NodeRef::new_comment(text))
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        if element.as_comment().is_none() {
            unreachable!("Non-comment node passed");
        }
        Self(element.clone())
    }

    /// Get the text in the comment
    pub fn text(&self) -> String {
        self.as_comment().unwrap().borrow().to_string()
    }

    /// Set different text in the comment
    pub fn set_text(&self, text: &str) {
        self.as_comment().unwrap().replace(text.into());
    }
}

/// Represents an internal link (`[[Foo|bar]]`)
/// ```
/// # use parsoid::prelude::*;
/// let text = Wikicode::new_text("baz");
/// let link = WikiLink::new("Foo bar", &text);
/// assert_eq!(
///     link.raw_target(),
///     "./Foo_bar".to_string()
/// );
/// assert_eq!(
///     link.target(),
///     "Foo bar".to_string()
/// );
/// assert_eq!(
///     link.text_contents(), "baz".to_string()
/// );
/// assert_eq!(
///     link.to_string(),
///     "<a href=\"./Foo_bar\" rel=\"mw:WikiLink\">baz</a>".to_string()
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Wiki_links) for more details.
#[derive(Debug, Clone)]
pub struct WikiLink(NodeRef);

impl WikiLink {
    const REL: &'static str = "mw:WikiLink";
    pub(crate) const SELECTOR: &'static str = "[rel=\"mw:WikiLink\"]";

    /// Create a new wiki link
    pub fn new(target: &str, text: &NodeRef) -> Self {
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("a")),
            vec![
                (
                    ExpandedName::new(ns!(), local_name!("href")),
                    Attribute {
                        prefix: None,
                        value: full_link(target),
                    },
                ),
                (
                    ExpandedName::new(ns!(), local_name!("rel")),
                    Attribute {
                        prefix: None,
                        value: Self::REL.to_string(),
                    },
                ),
            ],
        );
        element.append(text.clone());
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the raw link target, usually prefixed with `./`
    pub fn raw_target(&self) -> String {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("href")
            .unwrap()
            .to_string()
    }

    /// Get the link target (basically the page title)
    pub fn target(&self) -> String {
        parse_target(&self.raw_target())
    }

    /// Set the link target.
    pub fn set_target(&self, target: &str) {
        self.as_element()
            .unwrap()
            .attributes
            .borrow_mut()
            .insert("href", full_link(target));
    }

    /// Is this an ISBN magic link?
    /// See [Help:Magic links](https://www.mediawiki.org/wiki/Help:Magic_links).
    pub fn is_isbn_magic_link(&self) -> bool {
        // T329347: Parsoid doesn't currently mark up ISBN magic links with a
        // class, but we can detect it by looking for the lack of a title=
        // attribute and that the text contents start with "ISBN ", which is
        // always hardcoded English.
        !self
            .as_element()
            .unwrap()
            .attributes
            .borrow()
            .contains("title")
            && self.text_contents().starts_with("ISBN ")
    }
}

/// Represents an external link (`[https://example.org/ Text]`)
/// ```
/// # use parsoid::prelude::*;
/// let text = Wikicode::new_text("Text");
/// let link = ExtLink::new("https://example.org/", &text);
/// assert_eq!(
///     link.target(),
///     "https://example.org/".to_string()
/// );
/// assert_eq!(
///     link.text_contents(), "Text".to_string()
/// );
/// assert_eq!(
///     link.to_string(),
///     "<a href=\"https://example.org/\" rel=\"mw:ExtLink\">Text</a>".to_string()
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#External_links) for more details.
#[derive(Debug, Clone)]
pub struct ExtLink(NodeRef);

impl ExtLink {
    const REL: &'static str = "mw:ExtLink";
    pub(crate) const SELECTOR: &'static str = "[rel~=\"mw:ExtLink\"]";

    /// Create a new external link
    pub fn new(target: &str, text: &NodeRef) -> Self {
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("a")),
            vec![
                (
                    ExpandedName::new(ns!(), local_name!("href")),
                    Attribute {
                        prefix: None,
                        value: target.to_string(),
                    },
                ),
                (
                    ExpandedName::new(ns!(), local_name!("rel")),
                    Attribute {
                        prefix: None,
                        value: Self::REL.to_string(),
                    },
                ),
            ],
        );
        element.append(text.clone());
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the link target
    pub fn target(&self) -> String {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("href")
            .unwrap()
            .to_string()
    }

    /// Set a new link target
    pub fn set_target(&self, target: &str) {
        self.as_element()
            .unwrap()
            .attributes
            .borrow_mut()
            .insert("href", target.to_string());
    }

    /// Is this a PMID or RFC magic link?
    /// See [Help:Magic links](https://www.mediawiki.org/wiki/Help:Magic_links).
    pub fn is_magic_link(&self) -> bool {
        // T329347: Look if it contains the "mw-magiclink" class
        if let Some(classes) =
            self.as_element().unwrap().attributes.borrow().get("class")
        {
            classes.split(' ').any(|class| class == "mw-magiclink")
        } else {
            false
        }
    }
}

/// Represents an interwiki (non-language) link (`[[:en:Foo]]`)
/// ```
/// # use parsoid::prelude::*;
/// let text = Wikicode::new_text("en:Foo");
/// let link = InterwikiLink::new("https://en.wikipedia.org/wiki/Foo", &text);
/// assert_eq!(
///     link.target(),
///     "https://en.wikipedia.org/wiki/Foo".to_string()
/// );
/// assert_eq!(
///     link.text_contents(), "en:Foo".to_string()
/// );
/// assert_eq!(
///     &link.to_string(),
///     "<a href=\"https://en.wikipedia.org/wiki/Foo\" rel=\"mw:WikiLink/Interwiki\">en:Foo</a>"
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Interwiki_non-language_links) for more details.
#[derive(Debug, Clone)]
pub struct InterwikiLink(NodeRef);

impl InterwikiLink {
    const REL: &'static str = "mw:WikiLink/Interwiki";
    // pub(crate) const SELECTOR: &'static str = "[rel=\"mw:WikiLink/Interwiki\"]";

    /// Create a new interwiki link
    pub fn new(target: &str, text: &NodeRef) -> Self {
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("a")),
            vec![
                (
                    ExpandedName::new(ns!(), local_name!("href")),
                    Attribute {
                        prefix: None,
                        value: target.to_string(),
                    },
                ),
                (
                    ExpandedName::new(ns!(), local_name!("rel")),
                    Attribute {
                        prefix: None,
                        value: Self::REL.to_string(),
                    },
                ),
            ],
        );
        element.append(text.clone());
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the link target
    pub fn target(&self) -> String {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("href")
            .unwrap()
            .to_string()
    }

    /// Set a new link target
    pub fn set_target(&self, target: &str) {
        self.as_element()
            .unwrap()
            .attributes
            .borrow_mut()
            .insert("href", target.to_string());
    }
}

/// A `<nowiki>` tag
///
/// ```
/// # use parsoid::prelude::*;
/// let nowiki = Nowiki::new("plain [[wikitext]]");
/// assert_eq!(
///     &nowiki.to_string(),
///     "<span typeof=\"mw:Nowiki\">plain [[wikitext]]</span>"
/// );
/// assert_eq!(
///     &nowiki.text_contents(),
///     "plain [[wikitext]]"
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Nowiki_blocks) for more details.
#[derive(Debug, Clone)]
pub struct Nowiki(NodeRef);

impl Nowiki {
    const TYPEOF: &'static str = "mw:Nowiki";
    // pub(crate) const SELECTOR: &'static str = "[typeof=\"mw:Nowiki\"]";

    /// Create a `<nowiki>` tag
    pub fn new(text: &str) -> Self {
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("span")),
            vec![(
                ExpandedName::new(ns!(), "typeof"),
                Attribute {
                    prefix: None,
                    value: Self::TYPEOF.to_string(),
                },
            )],
        );
        element.append(NodeRef::new_text(text));
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }
}

/// An HTML entity that shouldn't be decoded during transformation
///
/// ```
/// # use parsoid::prelude::*;
/// // This is equal to &nbsp;
/// let entity = HtmlEntity::new("\u{a0}");
/// assert_eq!(
///     &entity.to_string(),
///     "<span typeof=\"mw:Entity\">&nbsp;</span>"
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#HTML_entities) for more details.
#[derive(Debug, Clone)]
pub struct HtmlEntity(NodeRef);

impl HtmlEntity {
    const TYPEOF: &'static str = "mw:Entity";
    // pub(crate) const SELECTOR: &'static str = "[typeof=\"mw:Entity\"]";

    pub fn new(text: &str) -> Self {
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("span")),
            vec![(
                ExpandedName::new(ns!(), "typeof"),
                Attribute {
                    prefix: None,
                    value: Self::TYPEOF.to_string(),
                },
            )],
        );
        element.append(NodeRef::new_text(text));
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }
}

/// Section contains a `Heading` and its contents. This only covers sections
/// generated by `== headings ==`, and not any `<section>` tag that might be
/// present in the output (e.g. generated by an extension).
///
/// It is not expected that this node will be created manually.
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Headings_and_Sections) for more details.
#[derive(Debug, Clone)]
pub struct Section(NodeRef);

impl Section {
    pub(crate) const SELECTOR: &'static str = "section[data-mw-section-id]";

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the section id (used by `action=edit` in section edits).
    ///
    /// This could be `0` (lead) or a greater integer.
    /// `-1` indicates an uneditable non-pseudo section, `-2` is an
    /// uneditable pseudo section
    pub fn section_id(&self) -> i32 {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("data-mw-section-id")
            // expect: Unreachable because this attribute is checked everywhere
            // this node type is created
            .expect("No data-mw-section-id attribute on section")
            .parse()
            // expect: This should be safe assuming we were provided
            // well-formed HTML.
            .expect("Invalid data-mw-section-id attribute")
    }

    pub fn is_editable(&self) -> bool {
        self.section_id() >= 0
    }

    pub fn is_pseudo_section(&self) -> bool {
        let id = self.section_id();
        id == -2 || id == 0
    }

    pub fn heading(&self) -> Option<Heading> {
        if !self.is_pseudo_section() {
            self.select_first(Heading::SELECTOR)
                .map(|node| Heading::new_from_node(&node))
        } else {
            None
        }
    }
}

/// A section heading (`== Some text ==`)
///
/// ```
/// # use parsoid::prelude::*;
/// # fn main() -> Result<()> {
/// let heading = Heading::new(2, &Wikicode::new_text("Some text"))?;
/// assert_eq!(
///     &heading.to_string(),
///     "<h2>Some text</h2>"
/// );
/// assert_eq!(heading.level(), 2);
/// # Ok(())
/// # }
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Headings_and_Sections) for more details.
#[derive(Debug, Clone)]
pub struct Heading(NodeRef);

impl Heading {
    pub(crate) const SELECTOR: &'static str = "h1, h2, h3, h4, h5, h6";

    pub fn new(level: u32, contents: &NodeRef) -> Result<Self> {
        if !(1..=6).contains(&level) {
            return Err(Error::InvalidHeadingLevel(level));
        }
        let element = NodeRef::new_element(
            crate::build_qual_name(format!("h{level}").into()),
            vec![],
        );
        element.append(contents.clone());
        Ok(Self(element))
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the numerical level of the heading, 1-6
    pub fn level(&self) -> u32 {
        match self.as_element().unwrap().name.local {
            local_name!("h1") => 1,
            local_name!("h2") => 2,
            local_name!("h3") => 3,
            local_name!("h4") => 4,
            local_name!("h5") => 5,
            local_name!("h6") => 6,
            _ => unreachable!("Non h[1-6] used in Heading"),
        }
    }
}

/// Represents a category link (`[[Category:Foo]]`)
/// ```
/// # use parsoid::prelude::*;
/// let cat = Category::new("Category:Foo bar", None);
/// assert_eq!(
///     &cat.category(),
///     "Category:Foo bar"
/// );
/// assert_eq!(
///     &cat.to_string(),
///     "<link href=\"./Category:Foo_bar\" rel=\"mw:PageProp/Category\">"
/// );
/// // Set a sort key
/// cat.set_sort_key(Some("Bar baz #quux"));
/// assert_eq!(
///     &cat.to_string(),
///     "<link href=\"./Category:Foo_bar#Bar%20baz%20%23quux\" rel=\"mw:PageProp/Category\">"
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Category_links) for more details.
#[derive(Debug, Clone)]
pub struct Category(NodeRef);

impl Category {
    const REL: &'static str = "mw:PageProp/Category";
    pub(crate) const SELECTOR: &'static str = "[rel=\"mw:PageProp/Category\"]";

    /// Create a new category link
    pub fn new(category: &str, sortkey: Option<&str>) -> Self {
        let href = Self::build_href(
            &full_link(category),
            sortkey.map(encode).as_deref(),
        );
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("link")),
            vec![
                (
                    ExpandedName::new(ns!(), local_name!("href")),
                    Attribute {
                        prefix: None,
                        value: href,
                    },
                ),
                (
                    ExpandedName::new(ns!(), local_name!("rel")),
                    Attribute {
                        prefix: None,
                        value: Self::REL.to_string(),
                    },
                ),
            ],
        );
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    fn split_href(&self) -> (String, Option<String>) {
        let href = self
            .as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("href")
            .unwrap()
            .to_string();
        let sp: Vec<_> = href.splitn(2, '#').collect();
        if sp.len() == 2 {
            (sp[0].to_string(), Some(sp[1].to_string()))
        } else {
            (sp[0].to_string(), None)
        }
    }

    fn build_href(category: &str, sortkey: Option<&str>) -> String {
        match sortkey {
            Some(sortkey) => {
                format!("{category}#{sortkey}")
            }
            None => category.to_string(),
        }
    }

    /// Get the category name
    pub fn category(&self) -> String {
        let (category, _) = self.split_href();
        clean_link(&category)
    }

    /// Get the sort key, if one is set
    pub fn sort_key(&self) -> Option<String> {
        let (_, sort_key) = self.split_href();
        // TODO: should we propagate this error instead of panic-ing?
        sort_key.map(|key| {
            decode(&key).expect("Unable to decode sort key").to_string()
        })
    }

    /// Set a different category name
    pub fn set_category(&self, category: &str) {
        let (_, sort_key) = self.split_href();
        self.set_href(&Self::build_href(
            &full_link(category),
            sort_key.as_deref(),
        ));
    }

    /// Set a different sort key
    pub fn set_sort_key(&self, sort_key: Option<&str>) {
        let (category, _) = self.split_href();
        self.set_href(&Self::build_href(
            &category,
            sort_key.map(encode).as_deref(),
        ));
    }

    fn set_href(&self, href: &str) {
        self.as_element()
            .unwrap()
            .attributes
            .borrow_mut()
            .insert("href", href.to_string());
    }
}

/// Represents a language link (`[[en:Foo]]`)
/// ```
/// # use parsoid::prelude::*;
/// let link = LanguageLink::new("https://en.wikipedia.org/wiki/Foo");
/// assert_eq!(
///     &link.target(),
///     "https://en.wikipedia.org/wiki/Foo"
/// );
/// assert_eq!(
///     &link.to_string(),
///     "<link href=\"https://en.wikipedia.org/wiki/Foo\" rel=\"mw:PageProp/Language\">"
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Language_links) for more details.
#[derive(Debug, Clone)]
pub struct LanguageLink(NodeRef);

impl LanguageLink {
    const REL: &'static str = "mw:PageProp/Language";
    // pub(crate) const SELECTOR: &'static str = "[rel=\"mw:PageProp/Language\"]";

    /// Create a new external link
    pub fn new(target: &str) -> Self {
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("link")),
            vec![
                (
                    ExpandedName::new(ns!(), local_name!("href")),
                    Attribute {
                        prefix: None,
                        value: target.to_string(),
                    },
                ),
                (
                    ExpandedName::new(ns!(), local_name!("rel")),
                    Attribute {
                        prefix: None,
                        value: Self::REL.to_string(),
                    },
                ),
            ],
        );
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the link target
    pub fn target(&self) -> String {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("href")
            .unwrap()
            .to_string()
    }

    /// Set a new link target
    pub fn set_target(&self, target: &str) {
        self.as_element()
            .unwrap()
            .attributes
            .borrow_mut()
            .insert("href", target.to_string());
    }
}

/// Represents a behavior switch (e.g. `__TOC__`, `{{DISPLAYTITLE:foo}}`)
/// ```
/// # use parsoid::prelude::*;
/// // __TOC__
/// let switch = BehaviorSwitch::new("toc", None);
/// assert_eq!(
///     &switch.property(),
///     "toc"
/// );
/// assert_eq!(switch.content(), None);
/// assert_eq!(
///     &switch.to_string(),
///     "<meta property=\"mw:PageProp/toc\">"
/// );
/// ```
///
/// Some behavior switches have some associated content
/// ```
/// # use parsoid::prelude::*;
/// // {{DISPLAYTITLE:foo}}
/// let switch = BehaviorSwitch::new("displaytitle", Some("foo"));
/// assert_eq!(
///     switch.content(),
///     Some("foo".to_string())
/// );
/// assert_eq!(
///     &switch.to_string(),
///     "<meta content=\"foo\" property=\"mw:PageProp/displaytitle\">"
/// );
/// switch.set_content("bar");
/// assert_eq!(
///     switch.content(),
///     Some("bar".to_string())
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Behavior_switches) for more details.
#[derive(Debug, Clone)]
pub struct BehaviorSwitch(NodeRef);

impl BehaviorSwitch {
    /// Create a new behavior switch
    pub fn new(property: &str, content: Option<&str>) -> Self {
        let mut attributes = vec![(
            ExpandedName::new(ns!(), local_name!("property")),
            Attribute {
                prefix: None,
                value: format!("mw:PageProp/{property}"),
            },
        )];
        if let Some(content) = content {
            attributes.push((
                ExpandedName::new(ns!(), local_name!("content")),
                Attribute {
                    prefix: None,
                    value: content.to_string(),
                },
            ));
        }
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("meta")),
            attributes,
        );
        Self(element)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the property name
    pub fn property(&self) -> String {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("property")
            .unwrap()
            .trim_start_matches("mw:PageProp/")
            .to_string()
    }

    pub fn content(&self) -> Option<String> {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("content")
            .map(|content| content.to_string())
    }

    pub fn set_content(&self, content: &str) {
        self.as_element()
            .unwrap()
            .attributes
            .borrow_mut()
            .insert("content", content.to_string());
    }
}

/// Represents a redirect (`#REDIRECT [[Foo]]`)
/// ```
/// # use parsoid::prelude::*;
/// let link = Redirect::new("Foo bar");
/// assert_eq!(
///     &link.target(),
///     "Foo bar"
/// );
/// assert_eq!(
///     &link.raw_target(),
///     "./Foo_bar"
/// );
/// assert_eq!(
///     &link.to_string(),
///     "<link href=\"./Foo_bar\" rel=\"mw:PageProp/redirect\">"
/// );
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Redirects) for more details.
#[derive(Debug, Clone)]
pub struct Redirect(NodeRef);

impl Redirect {
    const REL: &'static str = "mw:PageProp/redirect";
    pub(crate) const SELECTOR: &'static str = "[rel=\"mw:PageProp/redirect\"]";

    /// Create a new external link
    pub fn new(target: &str) -> Self {
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("link")),
            vec![
                (
                    ExpandedName::new(ns!(), local_name!("href")),
                    Attribute {
                        prefix: None,
                        value: "".to_string(),
                    },
                ),
                (
                    ExpandedName::new(ns!(), local_name!("rel")),
                    Attribute {
                        prefix: None,
                        value: Self::REL.to_string(),
                    },
                ),
            ],
        );
        let redirect = Self(element);
        redirect.set_target(target);
        redirect
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    pub fn is_external(&self) -> bool {
        !self.raw_target().starts_with("./")
    }

    /// Get the raw link target, usually beginning with `./`
    pub fn raw_target(&self) -> String {
        self.as_element()
            .unwrap()
            .attributes
            .borrow()
            .get("href")
            .unwrap()
            .to_string()
    }

    /// Get the link target, usually a page title but might also
    /// be an external URL
    pub fn target(&self) -> String {
        let raw = self.raw_target();
        if raw.starts_with("./") {
            clean_link(&raw)
        } else {
            raw
        }
    }

    /// Set a new link target
    pub fn set_target(&self, target: &str) {
        let new = if target.starts_with("http://")
            || target.starts_with("https://")
        {
            target.to_string()
        } else {
            full_link(target)
        };
        self.as_element()
            .unwrap()
            .attributes
            .borrow_mut()
            .insert("href", new);
    }
}

/// Represents a includeonly tag (`<includeonly>`)
///
/// ```
/// # use parsoid::prelude::*;
/// # fn main() -> Result<()> {
/// let includeonly = IncludeOnly::new("foo bar")?;
/// assert_eq!(
///     &includeonly.wikitext()?,
///     "foo bar"
/// );
/// assert_eq!(
///     &includeonly.to_string(),
///     "<meta data-mw=\"{&quot;src&quot;:&quot;<includeonly>foo bar</includeonly>&quot;}\" typeof=\"mw:Includes/IncludeOnly\">"
/// );
/// includeonly.set_wikitext("bar foo")?;
/// assert_eq!(
///     &includeonly.to_string(),
///     "<meta data-mw=\"{&quot;src&quot;:&quot;<includeonly>bar foo</includeonly>&quot;}\" typeof=\"mw:Includes/IncludeOnly\">"
/// );
/// # Ok(())
/// # }
/// ```
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#noinclude_/_includeonly_/_onlyinclude) for more details.
///
/// Note that while the spec has two nodes, one opening and one closing
/// (`typeof="mw:Includes/IncludeOnly/End"`), we only represent the opening
/// one since Parsoid automatically handles the lack of one or any extra one.
#[derive(Debug, Clone)]
pub struct IncludeOnly(NodeRef);

impl IncludeOnly {
    const TYPEOF: &'static str = "mw:Includes/IncludeOnly";
    // pub(crate) const SELECTOR: &'static str = "[typeof=\"mw:Includes/IncludeOnly\"]";

    pub fn new(wikitext: &str) -> Result<Self> {
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("meta")),
            vec![(
                ExpandedName::new(ns!(), "typeof"),
                Attribute {
                    prefix: None,
                    value: Self::TYPEOF.to_string(),
                },
            )],
        );
        let includeonly = Self(element);
        includeonly.set_wikitext(wikitext)?;
        Ok(includeonly)
    }

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    /// Get the wikitext contained inside the `<includeonly>` tag
    pub fn wikitext(&self) -> Result<String> {
        let data: IncludeOnlyDataMw = inner_data(self)?;
        Ok(data
            .src
            .strip_prefix("<includeonly>")
            .unwrap()
            .strip_suffix("</includeonly>")
            .unwrap()
            .to_string())
    }

    /// Set new wikitext to be inside the `<includeonly>` tag
    pub fn set_wikitext(&self, wikitext: &str) -> Result<()> {
        set_inner_data(
            self,
            crate::inclusion::IncludeOnlyDataMw {
                src: format!("<includeonly>{wikitext}</includeonly>"),
            },
        )
    }
}

/// A placeholder marks DOM content that may not be edited by clients.
/// This library does not take any steps to prevent mutation of these nodes,
/// it is expected that users will skip any Placeholder nodes encountered.
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Expectations_of_editing_clients) for more details.
#[derive(Debug, Clone)]
pub struct Placeholder(NodeRef);

impl Placeholder {
    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }
}

/// DisplaySpace is a non-breaking space that is added by Parsoid in
/// post-processing and is not present in wikitext.
///
/// See the [spec](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Display_space) for more details.
#[derive(Debug, Clone)]
pub struct DisplaySpace(NodeRef);

impl DisplaySpace {
    const TYPEOF: &'static str = "mw:DisplaySpace";

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }
}

/// Represents an indicator tag (`<indicator>`)
///
/// ```
/// # use parsoid::prelude::*;
/// # fn main() -> Result<()> {
///
/// let indicator = Indicator::new("test", "[[Some wikitext]]")?;
/// assert_eq!(&indicator.name()?, "test");
/// assert_eq!(&indicator.wikitext()?, "[[Some wikitext]]");
/// indicator.set_wikitext("[[Some other wikitext!]]")?;
/// assert_eq!(&indicator.wikitext()?, "[[Some other wikitext!]]");
/// # Ok(())
/// # }
/// ```
///
/// There is currently no published specification for this node.
#[derive(Debug, Clone)]
pub struct Indicator(NodeRef);

impl Indicator {
    const TYPEOF: &'static str = "mw:Extension/indicator";

    pub(crate) fn new_from_node(element: &NodeRef) -> Self {
        assert_element(element);
        Self(element.clone())
    }

    pub fn new(name: &str, wikitext: &str) -> Result<Self> {
        let data = IndicatorDataMw {
            name: "indicator".to_string(),
            attrs: IndicatorAttrs {
                name: name.to_string(),
            },
            body: IndicatorBody {
                extsrc: wikitext.to_string(),
            },
        };
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("link")),
            vec![
                (
                    ExpandedName::new(ns!(), "typeof"),
                    Attribute {
                        prefix: None,
                        value: Self::TYPEOF.to_string(),
                    },
                ),
                (
                    ExpandedName::new(ns!(), "data-mw"),
                    Attribute {
                        prefix: None,
                        value: serde_json::to_string(&data)?,
                    },
                ),
            ],
        );
        Ok(Self(element))
    }

    pub fn name(&self) -> Result<String> {
        Ok(self.inner()?.attrs.name)
    }

    pub fn set_name(&self, name: &str) -> Result<()> {
        let mut data = self.inner()?;
        data.attrs.name = name.to_string();
        self.set_inner(data)?;
        Ok(())
    }

    pub fn wikitext(&self) -> Result<String> {
        Ok(self.inner()?.body.extsrc)
    }

    pub fn set_wikitext(&self, wikitext: &str) -> Result<()> {
        let mut data = self.inner()?;
        data.body.extsrc = wikitext.to_string();
        self.set_inner(data)?;
        Ok(())
    }

    fn inner(&self) -> Result<IndicatorDataMw> {
        inner_data(self)
    }

    fn set_inner(&self, data: IndicatorDataMw) -> Result<()> {
        set_inner_data(self, data)
    }
}
