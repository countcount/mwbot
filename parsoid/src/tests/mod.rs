mod api;
mod gallery;
mod parsoid;
pub(crate) mod test_client;

use super::*;

#[test]
fn test_clean_link() {
    assert_eq!(
        clean_link("./Template:Foo_bar"),
        "Template:Foo bar".to_string()
    );
    assert_eq!(clean_link("ifeq"), "ifeq".to_string());
}

#[test]
fn test_full_link() {
    assert_eq!(
        full_link("Template:Foo bar"),
        "./Template:Foo_bar".to_string()
    );
    // Yes, this is not what you'd expect. It's not a direct reversal of clean_link()
    assert_eq!(full_link("ifeq"), "./ifeq".to_string());
}
