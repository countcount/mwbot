/*
Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::prelude::*;
use crate::tests::test_client;
use anyhow::Result;

#[tokio::test]
async fn test_gallery() -> Result<()> {
    let client = test_client::testwp_client();
    let code = client.get("Mwbot-rs/Gallery").await?.into_mutable();
    let galleries: Vec<_> = code
        .inclusive_descendants()
        .filter_map(|node| node.as_gallery())
        .collect();
    assert_eq!(galleries.len(), 1);
    assert_eq!(
        galleries[0].attributes()?.get("caption").unwrap(),
        "caption1"
    );
    assert!(galleries[0].wikitext()?.starts_with("File:Image.jpg"));
    let images = galleries[0].images();
    assert_eq!(images.len(), 3);
    assert_eq!(images[0].title(), "File:Image.jpg");
    assert_eq!(images[1].title(), "File:Foobar.jpg");
    assert_eq!(
        images[2].title(),
        "File:Drop of water on water-resistant textile (100% polyester).jpg"
    );
    Ok(())
}
