/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::{inner_data, map::IndexMap, set_inner_data, Result, WikiMultinode};
use kuchiki::{Attribute, ExpandedName, NodeRef};
use serde::{Deserialize, Serialize};
use std::fmt;

/// Represents a MediaWiki template (`{{foo}}`)
///
/// How to access values from an existing template:
/// ```
/// # use parsoid::prelude::*;
/// # #[tokio::main]
/// # async fn main() -> Result<()> {
/// # let client = ParsoidClient::new("https://www.mediawiki.org/api/rest_v1","parsoid-rs testing").unwrap();
/// let code = client.transform_to_html("{{1x|test}}").await?.into_mutable();
/// // Get the `Template` instance
/// let template = code.filter_templates()?[0].clone();
/// assert_eq!(template.name(), "Template:1x".to_string());
/// assert_eq!(template.raw_name(), "./Template:1x".to_string());
/// assert_eq!(template.name_in_wikitext(), "1x".to_string());
/// assert_eq!(template.param("1"), Some("test".to_string()));
/// # Ok(())
/// # }
/// ```
///
/// How to create and insert a new template:
/// ```
/// # use parsoid::prelude::*;
/// # #[tokio::main]
/// # async fn main() -> Result<()> {
/// # let client = ParsoidClient::new("https://www.mediawiki.org/api/rest_v1","parsoid-rs testing").unwrap();
/// let mut params = map::IndexMap::new();
/// params.insert("1".to_string(), "test".to_string());
/// let template = Template::new("1x", &params)?;
/// let code = Wikicode::new("");
/// code.append(&template);
/// let wikitext = client.transform_to_wikitext(&code).await?;
/// assert_eq!(wikitext, "{{1x|test}}".to_string());
/// # Ok(())
/// # }
/// ```
///
/// You can also use `Template::new_simple()` if there are no parameters to pass.
#[derive(Debug, Clone)]
pub struct Template {
    part: usize,
    element: NodeRef,
    siblings: Vec<NodeRef>,
}

impl Template {
    const TYPEOF: &'static str = "mw:Transclusion";
    pub(crate) const SELECTOR: &'static str = "[typeof~=\"mw:Transclusion\"]";

    /// Create a new template with no parameters
    pub fn new_simple(name: &str) -> Self {
        match Self::new(name, &IndexMap::new()) {
            Ok(temp) => temp,
            Err(e) => {
                // The only error condition currently reachable is if the parameters
                // can't be JSON serialized, which doesn't make sense for an empty map
                unreachable!(
                    "Template::new_simple() errored with {}",
                    e.to_string()
                );
            }
        }
    }

    /// Create a new template
    pub fn new(name: &str, params: &IndexMap<String, String>) -> Result<Self> {
        let params = params
            .iter()
            .map(|(key, val)| (key.to_string(), Param::new(val)))
            .collect();
        let transclusion = Transclusion {
            parts: vec![TransclusionPart::Template {
                template: TransclusionTemplate {
                    target: TransclusionTarget::new(name),
                    params,
                    i: 0,
                },
            }],
            errors: None,
        };
        let element = NodeRef::new_element(
            crate::build_qual_name(local_name!("span")),
            vec![
                (
                    ExpandedName::new(ns!(), "typeof"),
                    Attribute {
                        prefix: None,
                        value: Self::TYPEOF.to_string(),
                    },
                ),
                (
                    ExpandedName::new(ns!(), "data-mw"),
                    Attribute {
                        prefix: None,
                        value: serde_json::to_string(&transclusion)?,
                    },
                ),
            ],
        );
        Ok(Self {
            element,
            part: 0,
            siblings: vec![],
        })
    }

    pub(crate) fn new_from_node(
        element: &NodeRef,
        siblings: &[NodeRef],
        part: usize,
    ) -> Self {
        if element.as_element().is_none() {
            unreachable!("Non-element node passed");
        }
        Self {
            element: element.clone(),
            siblings: siblings.to_vec(),
            part,
        }
    }

    fn update(&self, temp: &TransclusionTemplate) -> Result<()> {
        let mut transclusion = self.transclusion();
        transclusion.parts[self.part] = TransclusionPart::Template {
            template: temp.clone(),
        };
        set_inner_data(&self.element, transclusion)
    }

    /// Remove the specified parameter from the template. If it was
    /// set, the previous value will be returned.
    pub fn remove_param(&self, name: &str) -> Result<Option<String>> {
        let mut transclusion = self.inner();
        let previous = transclusion.params.shift_remove(name);
        self.update(&transclusion)?;
        Ok(previous.map(|p| p.wt))
    }

    /// Set the specified wikitext as a template parameter with the given name
    ///
    /// The previous value, if one was set, will be returned.
    pub fn set_param(
        &self,
        name: &str,
        wikitext: &str,
    ) -> Result<Option<String>> {
        let mut transclusion = self.inner();
        let previous = transclusion
            .params
            .insert(name.to_string(), Param::new(wikitext));
        self.update(&transclusion)?;
        Ok(previous.map(|p| p.wt))
    }

    /// Override all parameters with the specified map. A map
    /// of all previously set parameters and their values will be returned.
    pub fn set_params(
        &self,
        map: impl Into<IndexMap<String, String>>,
    ) -> Result<IndexMap<String, String>> {
        let mut transclusion = self.inner();
        let previous = transclusion.params;
        let map = map.into();
        transclusion.params = map
            .into_iter()
            .map(|(key, v)| (key, Param::new(v)))
            .collect();
        self.update(&transclusion)?;
        Ok(previous.into_iter().map(|(k, v)| (k, v.wt)).collect())
    }

    fn transclusion(&self) -> Transclusion {
        inner_data(&self.element).expect("JSON parsing failed")
    }

    fn inner(&self) -> TransclusionTemplate {
        let transclusion = self.transclusion();

        if let TransclusionPart::Template { template: temp } =
            &transclusion.parts[self.part]
        {
            temp.clone()
        } else {
            unreachable!("Template part {} is the wrong type", self.part)
        }
    }

    /// Get the name of the template as it appears in wikitext
    pub fn name_in_wikitext(&self) -> String {
        self.inner().target.wt
    }

    /// Change the name of the template or parser function.
    /// Note that the `name` should be how the name in wikitext.
    /// For example, `"1x"` for a Template:1x,
    pub fn set_name(&self, name: String) -> Result<()> {
        let mut transclusion = self.inner();

        if self.is_template() {
            transclusion.target.wt = name;
            // Note that we don't also change target.href, it doesn't
            // appear to be necessary
        } else if self.is_parser_function() {
            transclusion.target.function = Some(name);
        } else {
            unreachable!("This is neither a template nor parser function, it should not happen")
        }

        self.update(&transclusion)
    }

    /// Get the full name of the template, e.g. `./Template:Foo_bar` or
    /// the parser function, e.g. `ifeq`.
    pub fn raw_name(&self) -> String {
        if self.is_template() {
            self.inner().target.href.unwrap()
        } else if self.is_parser_function() {
            self.inner().target.function.unwrap()
        } else {
            unreachable!("This is neither a template nor parser function, it should not happen")
        }
    }

    /// Get a pretty normalized name of a template, e.g. `Template:Foo bar` or
    /// the parser function, e.g. `ifeq`
    pub fn name(&self) -> String {
        if self.is_template() {
            urlencoding::decode(
                &self.raw_name().trim_start_matches("./").replace('_', " "),
            )
            .unwrap()
            .to_string()
        } else {
            self.raw_name()
        }
    }

    /// Get a map of all parameters, named and unnamed
    pub fn params(&self) -> IndexMap<String, String> {
        self.inner()
            .params
            .into_iter()
            .map(|(param, val)| (param, val.wt))
            .collect()
    }

    /// Get the wikitext value of a specific parameter if it exists
    pub fn param(&self, name: &str) -> Option<String> {
        self.inner().params.shift_remove(name).map(|val| val.wt)
    }

    /// Get the name of the parameter as it appears in the wikitext. For
    /// example given `{{1x|param<!--comment-->name=value}}` looking up
    /// `paramname` would return `Some("param<!--comment->name")`.
    pub fn param_in_wikitext(&self, name: &str) -> Option<String> {
        match self.inner().params.shift_remove(name) {
            Some(val) => match val.key {
                Some(keyval) => Some(keyval.wt),
                None => Some(name.to_string()),
            },
            None => None,
        }
    }

    /// Whether it's a template (as opposed to a parser function)
    pub fn is_template(&self) -> bool {
        self.inner().target.href.is_some()
    }

    /// Whether it's a parser function (as opposed to a template)
    pub fn is_parser_function(&self) -> bool {
        self.inner().target.function.is_some()
    }
}

impl WikiMultinode for Template {
    fn as_nodes(&self) -> Vec<NodeRef> {
        let mut nodes = vec![self.element.clone()];
        for node in &self.siblings {
            nodes.push(node.clone());
        }

        nodes
    }
}

impl fmt::Display for Template {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let nodes: Vec<_> = self
            .as_nodes()
            .iter()
            .map(|node| node.to_string())
            .collect();
        write!(f, "{}", nodes.join(""))
    }
}

/// Representation of [`mw:Transclusion`](https://www.mediawiki.org/wiki/Specs/HTML/2.3.0#Template_markup).
#[derive(Deserialize, Serialize, Clone, Debug)]
pub(crate) struct Transclusion {
    pub(crate) parts: Vec<TransclusionPart>,
    #[serde(skip_serializing_if = "Option::is_none")]
    errors: Option<Vec<TemplateError>>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
struct TemplateError {
    key: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    params: Option<Vec<String>>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(untagged)]
pub(crate) enum TransclusionPart {
    Template {
        template: TransclusionTemplate,
    },
    /// Representation of a "templatearg", see
    /// https://www.mediawiki.org/wiki/Specs/HTML/2.7.0#Parameter_Substitution_at_the_top-level
    ///
    /// These are not publicly exposed for modification.
    TemplateArg {
        templatearg: TransclusionTemplate,
    },
    /// Interspered wikitext is when "compound content blocks that include output from several transclusions"
    ///
    /// These are not publicly exposed for modification.
    InterspersedWikitext(String),
}

/// Representation of the `data-mw` part of `mw:Transclusion`.
#[derive(Deserialize, Serialize, Clone, Debug)]
pub(crate) struct TransclusionTemplate {
    target: TransclusionTarget,
    // Use a IndexMap to keep order
    params: IndexMap<String, Param>,
    i: i32, // index into data-parsoid->pi array for arg whitespace information for this template's params
}

#[derive(Deserialize, Serialize, Clone, Debug)]
struct TransclusionTarget {
    wt: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    href: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    function: Option<String>,
}

impl TransclusionTarget {
    fn new(name: &str) -> Self {
        Self {
            wt: name.to_string(),
            // html -> wikitext doesn't need `href`
            href: None,
            function: None,
        }
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
struct Param {
    wt: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    key: Option<Wt>,
}

impl Param {
    fn new(wikitext: impl Into<String>) -> Self {
        Param {
            wt: wikitext.into(),
            key: None,
        }
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
struct Wt {
    wt: String,
}
