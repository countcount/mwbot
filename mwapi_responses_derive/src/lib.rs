/*
Copyright (C) 2020-2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! mwapi_responses_derive
//! ======================
//!
//! This crate provides the proc_macro for [`mwapi_responses`](https://docs.rs/mwapi_responses/),
//! please refer to its documentation for usage.
#![deny(clippy::all)]

mod builder;
mod metadata;
mod params;
mod types;

extern crate proc_macro;
#[macro_use]
extern crate syn;

use crate::builder::{StructBuilder, StructField};
use crate::params::ParsedParams;
use crate::types::RustType;
use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use quote::{format_ident, quote};
use std::collections::{BTreeMap, HashMap};
use syn::{AttributeArgs, DeriveInput, Ident, NestedMeta, Visibility};

#[proc_macro_attribute]
pub fn query(args: TokenStream, input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let args = parse_macro_input!(args as AttributeArgs);
    let params = process_args(&args);
    let input = parse_macro_input!(input as DeriveInput);

    // Build the implementation
    impl_query(input, &params).into()
}

fn process_args(args: &[NestedMeta]) -> ParsedParams {
    let mut params = HashMap::new();
    for arg in args {
        if let NestedMeta::Meta(syn::Meta::NameValue(meta)) = arg {
            let name = meta.path.get_ident().unwrap().to_string();
            let val = match &meta.lit {
                syn::Lit::Str(val) => val.value(),
                _ => panic!("Non-string value found for parameter '{}'", &name),
            };
            params.insert(name, val);
        } else {
            panic!("Unexpected value");
        }
    }
    ParsedParams::new(params)
}

#[derive(Default)]
struct FieldContainer {
    top: BTreeMap<String, metadata::Field>,
    sub: HashMap<String, BTreeMap<String, metadata::Field>>,
}

impl FieldContainer {
    fn add_fields(
        &mut self,
        wrap_field: Option<String>,
        fields: Vec<metadata::Field>,
    ) {
        let map: BTreeMap<String, metadata::Field> = fields
            .into_iter()
            .map(|field| (field.name.to_string(), field))
            .collect();
        if let Some(fieldname) = wrap_field {
            self.sub.entry(fieldname).or_default().extend(map);
        } else {
            self.top.extend(map);
        }
    }
}

fn build_modules(
    prefix: &Ident,
    visibility: &Visibility,
    body_ident: &Ident,
    qparams: &ParsedParams,
) -> (Vec<StructBuilder>, String, Ident) {
    // dbg!(modules);
    let mut structs = vec![];
    let fieldname = qparams.get_fieldname();
    let container = qparams.get_fields();
    let mut top_fields: Vec<StructField> = container
        .top
        .into_values()
        .map(|field| field.into())
        .collect();
    for (subname, fields) in container.sub {
        let ident = format_ident!("{}Item{}", prefix, subname);
        let fields: Vec<StructField> =
            fields.into_values().map(|field| field.into()).collect();
        // dbg!(&fields);
        structs.push(StructBuilder {
            ident: ident.clone(),
            fields,
            visibility: visibility.clone(),
        });
        top_fields.push(StructField {
            name: subname,
            type_: RustType::VecIdent {
                ident: ident.to_string(),
            },
            default: true,
            rename: None,
            deserialize_with: None,
        });
    }

    let item_ident = format_ident!("{}{}", prefix, "Item");
    // dbg!(&container);
    structs.push(StructBuilder {
        ident: item_ident.clone(),
        fields: top_fields,
        visibility: visibility.clone(),
    });
    let body_fields = vec![
        StructField {
            name: fieldname.to_string(),
            type_: RustType::VecIdent {
                ident: item_ident.to_string(),
            },
            default: false,
            rename: None,
            deserialize_with: None,
        },
        StructField {
            name: "normalized".to_string(),
            type_: RustType::VecIdent {
                ident: "mwapi_responses::normalize::Normalized".to_string(),
            },
            default: true,
            rename: None,
            deserialize_with: None,
        },
        StructField {
            name: "redirects".to_string(),
            type_: RustType::VecIdent {
                ident: "mwapi_responses::normalize::Redirect".to_string(),
            },
            default: true,
            rename: None,
            deserialize_with: None,
        },
    ];
    structs.push(StructBuilder {
        ident: body_ident.clone(),
        fields: body_fields,
        visibility: visibility.clone(),
    });

    (structs, fieldname, item_ident)
}

fn impl_query(input: DeriveInput, qparams: &ParsedParams) -> TokenStream2 {
    let prefix = &input.ident;
    let visiblity = &input.vis;
    let body_ident = format_ident!("{}Body", prefix);
    let (mut structs, item_fieldname, item_ident) =
        build_modules(prefix, visiblity, &body_ident, qparams);
    let item_fieldname = format_ident!("{}", item_fieldname);
    // Add the main struct
    structs.push(StructBuilder {
        ident: prefix.clone(),
        fields: vec![
            StructField {
                name: "batchcomplete".to_string(),
                type_: RustType::Simple("bool".to_string()),
                default: true,
                rename: None,
                deserialize_with: None,
            },
            StructField {
                name: "continue".to_string(),
                type_: RustType::HashMap(crate::types::HashMap {
                    key: "String".to_string(),
                    value: "String".to_string(),
                }),
                default: true,
                rename: Some("continue_".to_string()),
                deserialize_with: None,
            },
            StructField {
                name: "query".to_string(),
                type_: RustType::Ident {
                    ident: body_ident.to_string(),
                },
                default: false,
                rename: None,
                deserialize_with: None,
            },
        ],
        visibility: visiblity.clone(),
    });

    quote! {
        impl ::mwapi_responses::ApiResponse<#item_ident> for #prefix {
            fn params() -> &'static [(&'static str, &'static str)] {
                #qparams
            }

            fn items(&self) -> ::std::slice::Iter<'_, #item_ident> {
                self.query.#item_fieldname.iter()
            }

            fn into_items(self) -> ::std::vec::IntoIter<#item_ident> {
                self.query.#item_fieldname.into_iter()
            }

            fn redirects(&self) -> &[::mwapi_responses::normalize::Redirect] {
                &self.query.redirects
            }

            fn normalized_titles(&self) -> &[::mwapi_responses::normalize::Normalized] {
                &self.query.normalized
            }

        }

        #(#structs)*
    }
}
