/*
Copyright (C) 2022 Deadbeef <ent3rm4n@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// On wasm32, we can't use `tokio::time`, so use the equivalent of
// `setTimeout()`.
// TODO: Handle wasm32-wasi, which *is* supported by tokio

#[cfg(target_arch = "wasm32")]
pub(crate) async fn sleep(secs: u64) {
    let promise = js_sys::Promise::new(&mut |yes, _| {
        web_sys::window()
            .unwrap()
            .set_timeout_with_callback_and_timeout_and_arguments_0(
                &yes,
                (secs * 1000) as i32,
            )
            .unwrap();
    });
    wasm_bindgen_futures::JsFuture::from(promise).await.unwrap();
}

#[cfg(not(target_arch = "wasm32"))]
pub(crate) async fn sleep(secs: u64) {
    tokio::time::sleep(tokio::time::Duration::from_secs(secs)).await;
}
